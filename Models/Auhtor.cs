using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace apiRest.Models
{
    public class Author
    {
        [Key]
        public int Id_Author { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Last_Name { get; set; }

        [Required]
        public string Email { get; set; }
    }
}