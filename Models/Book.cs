using Microsoft.EntityFrameworkCore;

namespace apiRest.Models
{
    [Keyless]
    public class Book
    {
        public int Id_Book { get; set; }
        public int Id_Author { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Section { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public string Publisher { get; set; }
    }
}