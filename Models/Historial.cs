using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace apiRest.Models
{
    [Table("historial")]
    public class Historial
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string palabra { get; set; }

        [Required]
        public string tipo { get; set; }

        public DateTime created_at { get; set; } = DateTime.Now;
    }
}