using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using apiRest.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace apiRest.Controllers
{
    [Route("/api/news")]
    public class NewsController : Controller
    {
        private readonly LibraryDbContext _context;

        public NewsController(LibraryDbContext context)
        {
            _context = context;
        }
        private const string token = "e5b550da5d524b89ae329358000a96ab";
        private const string url = "https://newsapi.org/v2/everything";
        private static readonly HttpClient client = new HttpClient();

        [EnableCors("Policy1")]
        [HttpGet("{param}")]
        public async Task<NewsRepository> get(string param)
        {
            Historial h = new Historial();
            h.palabra = param;
            h.tipo = "Noticias";
            
            this._context.Historial.Add(h);
            this._context.SaveChanges();

            var streamTask = client.GetStreamAsync(url + "?q=" + param + "&sortBy=publishedAt&apiKey=" + token);
            var repositories = await JsonSerializer.DeserializeAsync<NewsRepository>(await streamTask);

            return repositories;
        }
    }
}