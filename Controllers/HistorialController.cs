using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using apiRest.Models;
using Microsoft.AspNetCore.Mvc;

namespace apiRest.Controllers
{

    [Route("api/v1/[controller]")]
    public class HistorialController : Controller
    {
        private readonly LibraryDbContext _context;

        public HistorialController(LibraryDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Historial> Get()
        {
            return _context.Historial.ToList();

        }

        //Search by Name 
        [HttpGet("{name}")]
        public IActionResult GetHistorialByName(string name)
        {
            var info = this._context.Historial.SingleOrDefault(ct => ct.palabra == name);

            if (info == null)
            {
                return new NoContentResult();
            }
            else
            {
                return Ok(info);
            }
        }

        //AddHistory
        [HttpPost]
        public IActionResult AddHistorial([FromBody] Historial historial)
        {
            
            if (!this.ModelState.IsValid)
            {
                return BadRequest();
            }
            
            this._context.Historial.Add(historial);
            this._context.SaveChanges();
            return Created($"historial/{historial.id}", historial);
        }
    }
}