using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using apiRest.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace apiRest.Controllers
{
    [Route("/api/weather")]
    public class WeatherController : Controller
    {
        private readonly LibraryDbContext _context;

        public WeatherController(LibraryDbContext context)
        {
            _context = context;
        }

        private const string token = "6db68979c3a2d398eab9796654da1fb6";
        private const string url = "https://api.openweathermap.org/data/2.5/weather";
        private static readonly HttpClient client = new HttpClient();

        [EnableCors("Policy1")]
        [HttpGet("{param}")]
        public async Task<WeatherRepository> get(string param)
        {
            Historial h = new Historial();
            h.palabra = param;
            h.tipo = "Tiempo";
            
            this._context.Historial.Add(h);
            this._context.SaveChanges();
            
            var streamTask = client.GetStreamAsync(url + "?q=" + param + "&lang=es&units=metric&appid=" + token);
            var repositories = await JsonSerializer.DeserializeAsync<WeatherRepository>(await streamTask);

            return repositories;
        }
    }
}