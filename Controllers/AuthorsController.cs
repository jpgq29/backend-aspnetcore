using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using apiRest.Models;
using Microsoft.AspNetCore.Mvc;

namespace apiRest.Controllers
{

    [Route("api/v1/[controller]")]
    public class AuthorsController : Controller
    {
        private readonly LibraryDbContext _context;

        public AuthorsController(LibraryDbContext context)
        {
            _context = context;
        }

        [HttpGet("prueba")]
        public String getPrueba()
        {

            var url = $"https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=demo&count=2";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return "Sin datos";
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            // Do something with responseBody
                            Console.WriteLine(responseBody);
                            return responseBody;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                // Handle error
                return "Sin datos";
            }

        }

        [HttpGet]
        public List<Author> Get()
        {
            return _context.Authors.ToList();

        }

        //Search by ID
        [HttpGet("{id:int}")]
        public IActionResult GetAuthorById(int id)
        {

            var author = this._context.Authors.SingleOrDefault(ct => ct.Id_Author == id);
            if (author != null)
            {
                return Ok(author);
            }
            else
            {
                return NotFound();
            }

        }

        //Search by Name 

        [HttpGet("{name}")]
        public IActionResult GetAuthorByName(string name)
        {
            var info = this._context.Authors.SingleOrDefault(ct => ct.Name == name);

            if (info == null)
            {
                return new NoContentResult();
            }
            else
            {
                return Ok(info);
            }
        }

        //AddAuthors
        [HttpPost]
        public IActionResult AddAuthors([FromBody] Author author)
        {

            if (!this.ModelState.IsValid)
            {
                return BadRequest();
            }
            this._context.Authors.Add(author);
            this._context.SaveChanges();
            return Created($"authors/{author.Id_Author}", author);
        }

        //UpdateAuthors
        [HttpPut("{id}")]
        public IActionResult PutAuthors(int id, [FromBody] Author author)
        {

            var target = _context.Authors.FirstOrDefault(ct => ct.Id_Author == id);
            if (target == null)
            {
                return NotFound();
            }
            else
            {
                target.Id_Author = author.Id_Author;
                target.Name = author.Name;
                target.Last_Name = author.Last_Name;
                target.Email = author.Email;

                _context.Authors.Update(target);
                _context.SaveChanges();
                return new NoContentResult();
            }

        }

        //Delete Authors
        [HttpDelete("{id}")]
        public IActionResult DeleteAuthors(int id)
        {
            var target = _context.Authors.FirstOrDefault(ct => ct.Id_Author == id);
            if (!this.ModelState.IsValid)
            {
                return BadRequest();
            }
            else
            {
                _context.Authors.Remove(target);
                _context.SaveChanges();
                return Ok();
            }
        }
    }
}