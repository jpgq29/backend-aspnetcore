
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Repository
{
    public class WeatherRepository
    {
        [JsonPropertyName("weather")]
        public List<Weather> weather { get; set; }

        [JsonPropertyName("main")]
        public Main main { get; set; }

        [JsonPropertyName("name")]
        public string name { get; set; }
    }

    public class Weather
    {
        [JsonPropertyName("description")]
        public string description { get; set; }
    }

    public class Main
    {
        [JsonPropertyName("temp")]
        public double temp { get; set; }

        [JsonPropertyName("temp_min")]
        public double temp_min { get; set; }

        [JsonPropertyName("temp_max")]
        public double temp_max { get; set; }

        [JsonPropertyName("humidity")]
        public int humidity { get; set; }

    }
}