
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Repository
{
    public class NewsRepository{
        [JsonPropertyName("articles")]
        public List<Articles> articles { get; set; }
    }   


    public class Articles{
        public string title { get; set; }

        public string description { get; set; }

        public Uri url { get; set; }

        public Uri urlToImage { get; set; }
    }  
}